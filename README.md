## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/caddy/badges/main/pipeline.svg)](https://gitlab.com/bastillebsd-templates/caddy/commits/main)

## Caddy
Bastille template to bootstrap Caddy

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/caddy
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/caddy
```

## Problems
After you have installed and configured this software, you will eventually be surprsed to discover that it does not currently (Oct 2022) include caching.  There are two packages which can provide caching.  
[cache-handler](https://github.com/caddyserver/cache-handler) is the official solution, 
but sadly it is complex, and poorly documented.   [cdp-cache](https://github.com/sillygod/cdp-cache) is the original Caddy 1 caching software ported to Caddy 2.   It is easy to install and configure.  But sadly it is a bit out of date.  One needs to use golang version 1.17
Here are the [installation instructions](https://dev.to/jeremycmorgan/how-to-install-golang-in-freebsd-in-5-minutes-1862
)

xcaddy will help you build it. 

```
bastille pkg TARGET install xcaddy
```
or if you named your caddy container caddy:

```
bastille pkg caddy install xcaddy
```

And then you can build it. 

```
xcaddy build v2.4.6 --with github.com/sillygod/cdp-cache
```

Newer versions than 2.4.6 will not work. 

Eventually I expect that the caddy server guys will get their act together and include an easy way to do caching, but in the meantime, this is working fine for me. 

Please let us know how it goes.  Reliability comes from lots of people doing the same thing.  Even better would be if someone would be so kind as to author a template which does all of this automatically. 

